import React, {useEffect, useState } from 'react';

function ServiceHistory(props) {
    const [searchVin, setSearchVin] = useState('');
    const [appointments, setAppointments] = useState([])
    const handleSearch = async (event) => {
    const value = event.target.value
        setSearchVin(value)
    }
    async function fetchAppointments(){
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        const data = await response.json();
            if (response.ok) {
                const apptData = data.appointments
                const results = apptData.filter(appointment => appointment.vin === searchVin)
                    setAppointments(results)
            }
    }

  useEffect(() => {
     fetchAppointments();
  }, []);

return (
    <div>
        <div className="input-group">
            <input type="search" onChange={handleSearch} className="form-control rounded" placeholder="Search by VIN" aria-label="Search" aria-describedby="search-addon"/>
            <button className="btn btn-outline-success" onClick={fetchAppointments}  placeholder="Search by VIN" id="searchBar">Search</button>
        </div>
        <h1> Service History </h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Service Reason</th>
                    <th>Technician</th>
                </tr>
            </thead>
            <tbody>
            {appointments.map(appt => {
                return (
                    <tr key={appt.href}>
                        <td>{ appt.vin }</td>
                        <td>{ appt.customer_name }</td>
                        <td>{ new Date(appt.date).toLocaleDateString("en-US") }</td>
                        <td>{ new Date(appt.date).toLocaleTimeString([], {hour:"2-digit",minute:"2-digit"})}</td>
                        <td>{ appt.service_reason }</td>
                        <td>{ appt.technician.name }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    </div>

);
}


export default ServiceHistory;
