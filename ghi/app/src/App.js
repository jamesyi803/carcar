import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import React, {useEffect, useState} from 'react'
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import ModelList from './ModelList';
import SalesRecordList from './SalesRecordList';
import SalespersonSalesList from './SalespersonSalesList';
import CreateAppointmentForm from './CreateAppointmentForm';
import CreateTechnicianForm from './CreateTechnicianForm';
import AppointmentList from './AppointmentList'
import ServiceHistory from './ServiceHistory';
import CreateManufacturerForm from './CreateManufacturerForm';
import ManufacturersList from './ManufacturersList';
import CreateVehicleForm from './CreateVehicleForm';


export default function App(props) {
  const [automobiles, setAutomobiles] = useState([])
  const [models, setModels] = useState([])
  const [sales, setSales] = useState([])
  const [salespersons, setSalespersons] = useState([])
  const [appointment, setAppointment] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);

  const getAutomobiles = async () => {
    const automobileUrl = "http://localhost:8100/api/automobiles/"
    const response = await fetch(automobileUrl)

    if (response.ok) {
      const data = await response.json()
      const automobiles = data.autos
      setAutomobiles(automobiles)
    }
  }
  const getModels = async () => {
    const modelUrl = "http://localhost:8100/api/models/"
    const response = await fetch(modelUrl)

    if (response.ok) {
      const data = await response.json()
      const models = data.models
      setModels(models)
    }
  }
  const getSales = async () => {
    const salesUrl = "http://localhost:8090/api/sales/"
    const response = await fetch(salesUrl)

    if (response.ok) {
      const data = await response.json()
      const sales = data.sales_records
      setSales(sales)
    }
  }
  const getSalespersons = async () => {
    const salespersonUrl = "http://localhost:8090/api/salespersons/"
    const response = await fetch(salespersonUrl)

    if (response.ok) {
      const data = await response.json()
      const salespersons = data.salespersons
      setSalespersons(salespersons)
    }
  }
  const getAppointment = async () => {
    const apptUrl ='http://localhost:8080/api/appointments/';
    const response = await fetch(apptUrl)

    if (response.ok) {
      const data = await response.json();
      setAppointment(data.appointments)
    }
  }
  const getManufactureres = async () => {
    const ManufacturersUrl ='http://localhost:8100/api/manufacturers/';
    const response = await fetch(ManufacturersUrl)

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }
  useEffect(() => {
    getAutomobiles();
    getModels();
    getSales();
    getSalespersons();
    getAppointment();
    getManufactureres();
  },[])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salesperson">
            <Route path="" element={<SalespersonSalesList salespersons={salespersons} getSalespersons={getSalespersons} sales={sales} getSales={getSales}/>} />
            <Route path="new" element={<SalespersonForm getSalespersons={getSalespersons} getSales={getSales}/>}/>
          </Route>
          <Route path="customer">
            <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path="sale">
            <Route path="" element={<SalesRecordList sales={sales} getSales={getSales}/>} />
            <Route path="new" element={<SalesRecordForm getSales={getSales} getAutomobiles={getAutomobiles}/>}/>
          </Route>
          <Route path="automobile">
            <Route path="" element={<AutomobileList automobiles={automobiles} getAutomobiles={getAutomobiles}/>} />
            <Route path="new" element={<AutomobileForm automobiles={automobiles} getAutomobiles={getAutomobiles}/>} />
          </Route>
          <Route path="model">
            <Route path="" element={<ModelList models={models} getModels={getModels}/>} />
            <Route path="new" element={<CreateVehicleForm getModels={getModels}/> } />
          </Route>
          <Route path="technicians" element={<CreateTechnicianForm />} />
          <Route path="appointments">
            <Route path="" element={<AppointmentList appointment={appointment} getAppointment={getAppointment} />} />
            <Route path="new" element={<CreateAppointmentForm getAppointment={getAppointment}/>} />
            <Route path="history" element={<ServiceHistory appointment={appointment} getAppointment={getAppointment} />} />
          </Route>
          <Route path="manufacturers" >
            <Route path="" element={<ManufacturersList manufacturers={manufacturers} getManufactureres={getManufactureres} /> } />
            <Route path="new" element={<CreateManufacturerForm getManufactureres={getManufactureres}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
