import React, {useEffect, useState} from 'react'

function CreateManufacturerForm(props) {
    const [manufacturerName, setManufacturerName] = useState('')


    const handleSubmit = async (event) => {
          event.preventDefault();
          const data = {};

          data.name = manufacturerName;

          const ManufacturerUrl = `http://localhost:8100/api/manufacturers/`
          const fetchConfig = {
              method: "post",
              body: JSON.stringify(data),
              headers: {
                  'Content-Type': 'application/json',
              },
          };

          const response = await fetch(ManufacturerUrl, fetchConfig);
          if (response.ok) {
              const newManufacturer = await response.json();
              console.log(newManufacturer);
              setManufacturerName('');
              props.getManufactureres()

          }
      }
      const handleNameChange = (event) => {
          const value = event.target.value;
          setManufacturerName(value);
      }

    return (
      <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Car Manufacturer</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} value={manufacturerName} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="nameChange">Manufacturer Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
     );
}

  export default CreateManufacturerForm;
