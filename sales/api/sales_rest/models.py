from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    availability = models.BooleanField(default=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)
    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecords",
        on_delete=models.PROTECT,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="salesrecords",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="salesrecords",
        on_delete=models.PROTECT,
    )
    price = models.PositiveIntegerField()

    def __str__(self):
        return str(self.salesperson)
